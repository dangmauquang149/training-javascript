class Student {
    constructor(ID, firstName, lastName, scores = {}) {
        this.ID = ID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.scores = scores;
    }
    get averageScore() {
        let sum = 0, count = 0;
        Object.values(this.scores).forEach((element, index) => {
            sum += element;
            count = index + 1;
        });
        return sum / count;
    }
    get StudentTilte() {
        arr1 = Object.values(this.scores);
        arr1.sort((a, b) => a < b ? -1 : a > b ? 1 : 0);
        switch (true) {
            case this.averageScore < 5:
                console.log("Bad student");
                break;
            case this.averageScore < 6.5:
                switch (true) {
                    case arr1[0] < 3.5:
                        console.log("Bad student");
                        break;
                    default:
                        console.log("Average student");
                        break;
                }
                break;
            case this.averageScore < 8:
                switch (true) {
                    case arr1[0] < 3.5:
                        console.log("Bad student");
                        break;
                    case arr1[0] < 5:
                        console.log("Average student");
                        break;
                    default:
                        console.log("Good student");
                        break;
                }
                break
            default:
                switch (true) {
                    case arr1[0] < 3.5:
                        console.log("Bad student");
                        break;
                    case arr1[0] < 5:
                        console.log("Average student");
                        break;
                    case arr1[0] < 6.5:
                        console.log("Good student");
                        break;
                    default:
                        console.log("Excelent student");
                        break;
                }
                break;
        }
    }
    static bestStudents(students) {
        return students.sort((a, b) => a.averageScore < b.averageScore)
            .splice(0, Math.ceil(students.length / 10));
    }
};
var arr1 = [];
var a;
let student1 = [new Student(1, "Le Huy", "Truyen", { math: 8, physical: 7, chemistry: 9.5 }),
new Student(2, "Ngo Ba", "Kha", { math: 6, physical: 9, chemistry: 2 }),
new Student(3, "Duong Minh", "Tuyen", { math: 10, physical: 8, chemistry: 10 }),
new Student(4, "Phu", "Le", { math: 6, physical: 6, chemistry: 6 })
]
// student1.forEach(element => {
//     console.log(`${element.firstName} ${element.lastName} has averageScore = ${element.averageScore} and title:`);
//     element.StudentTilte

// });

console.log(Student.bestStudents(student1));



