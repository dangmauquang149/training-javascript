class Animal {
    constructor(name, age, spec) {
        this.name = name;
        this.age = age;
        this.spec = spec;
    }

    get getAge() {
        return this.age;
    }

    static sumAnimalAge(cat1, cat2) {
        return cat1.age + cat2.age;
    }

}

class Dog extends Animal {
    constructor(name, age, gender, score, spec = "dog") {
        super(name, age, spec);
        this.gender = gender;
        this.score = score;
    }

    speak() {
        return "Whow whow";
    }
}

let cat1 = new Animal("paw", 2, "cat");
let cat2 = new Animal("pew", 3, "cat");
let dog1 = new Dog('milu', 5, { math: 10, physical: 10, }, "male")

console.log(dog1);

console.log(cat1);
//Check cat1
cat1 instanceof Animal
//Call method speak()
//
console.log(cat1.getAge);

console.log(Animal.sumAnimalAge(cat1, cat2));

Animal.prototype.color = "black";

console.log(cat1.color);

console.log(cat1.hasOwnProperty("color"));









